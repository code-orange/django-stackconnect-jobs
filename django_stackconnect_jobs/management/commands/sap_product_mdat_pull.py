import django

django.setup()

import multiprocessing as mp

from django.core.management.base import BaseCommand

from django_mdat_customer.django_mdat_customer.models import MdatItems
from django_sap_business_one_models.django_sap_business_one_models.models import (
    Oitm,
    Itm1,
)
from django_stackconnect_jobs.django_stackconnect_jobs.tasks import connect_databases
from django_stackconnect_main.django_stackconnect_main.models import CsExternalDb

sap_database = CsExternalDb.objects.get(db_type_id=1, id=1)
mdat_database = CsExternalDb.objects.get(db_type_id=20, id=15)
connect_databases([sap_database, mdat_database])


def update_item_details(item_id: int):
    item = MdatItems.objects.using("masterdata").get(id=item_id)

    sap_item = (
        Oitm.objects.using("DOLPROD")
        .filter(itemcode=item.external_id)
        .update(
            itemname=item.name[:200],
            usertext=item.description_text,
            u_co_mdat_id=item.id,
        )
    )
    sap_price = (
        Itm1.objects.using("DOLPROD")
        .filter(itemcode=item.external_id)
        .update(
            price=item.price,
        )
    )

    return True


class Command(BaseCommand):
    def handle(self, *args, **options):
        pool = mp.Pool(mp.cpu_count())

        items = list(
            MdatItems.objects.using(mdat_database.name)
            .filter(created_by_id=100005)
            .values_list("id", flat=True)
            .distinct()
        )

        pool.map(update_item_details, items)

        pool.close()
