import json
import urllib.parse

import requests
from django.conf import settings
from django.core.management.base import BaseCommand

from django_sap_business_one_models.django_sap_business_one_models.models import *


def RepresentsInt(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


class Command(BaseCommand):
    help = "Sync product with ITscope"

    def handle(self, *args, **options):
        # New Installation Date
        self.stdout.write(self.help)

        prod_wtho_itscopeid = (
            Oitm.objects.using("sapserver")
            .filter(usertext=None)
            .exclude(u_itscopeid=None)
            .order_by("itemcode")
        )

        for item in prod_wtho_itscopeid:
            apiurl = (
                "https://api.itscope.com/2.0/products/id/"
                + str(urllib.parse.quote(item.u_itscopeid, safe=""))
                + "/standard.json?realtime=false"
            )
            r = requests.get(
                apiurl,
                auth=(settings.ITSCOPE_API_AUTH_USER, settings.ITSCOPE_API_AUTH_PASSWD),
            )

            statuscode = r.status_code

            if statuscode == 200:
                json_data = json.loads(r.text)
                item.usertext = json_data["product"][0]["longDescription"]
                item.save()

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
