import json
import urllib.parse

import requests
from django.conf import settings
from django.core.management.base import BaseCommand

from django_sap_business_one_models.django_sap_business_one_models.models import *


def RepresentsInt(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


class Command(BaseCommand):
    help = "Get similar products to match them for their EAN"

    def handle(self, *args, **options):
        # New Installation Date
        self.stdout.write(self.help)

        prod_wtho_itscopeid = (
            Oitm.objects.using("sapserver")
            .filter(u_itscopeid=None)
            .exclude(codebars=None)
            .order_by("itemcode")
        )

        for item in prod_wtho_itscopeid:
            apiurl = (
                "https://api.itscope.com/2.0/products/ean/"
                + str(urllib.parse.quote(item.codebars, safe=""))
                + "/standard.json?realtime=false&plzproducts=false"
            )
            r = requests.get(
                apiurl,
                auth=(settings.ITSCOPE_API_AUTH_USER, settings.ITSCOPE_API_AUTH_PASSWD),
            )

            statuscode = r.status_code

            if statuscode == 200:
                json_data = json.loads(r.text)
                item.u_itscopeid = json_data["product"][0]["puid"]
                item.save()

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
