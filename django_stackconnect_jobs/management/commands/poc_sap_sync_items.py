from django.core.management.base import BaseCommand
from django.conf import settings

from django_mdat_customer.django_mdat_customer.models import MdatItems
from django_sap_business_one_client.django_sap_business_one_client.func import SboClient
from django_sap_business_one_models.django_sap_business_one_models.models import Oitm
from django_stackconnect_jobs.django_stackconnect_jobs.tasks import connect_databases
from django_stackconnect_main.django_stackconnect_main.models import *


class Command(BaseCommand):
    def handle(self, *args, **options):
        sap_api = CsExternalApi.objects.get(id=1)
        sap_database = CsExternalDb.objects.get(id=1)
        mdat = CsExternalDb.objects.get(id=15)

        # sync items missing in SAP
        connect_databases([sap_database, mdat])

        known_items_sap = list(
            Oitm.objects.using(sap_database.name)
            .all()
            .values_list("itemcode", flat=True)
            .distinct()
        )

        print("KNOWN SAP: " + str(len(known_items_sap)))

        new_items = list(
            MdatItems.objects.using(mdat.name)
            .filter(created_by_id=settings.MDAT_ROOT_CUSTOMER_ID)
            .exclude(external_id__in=known_items_sap)
        )

        print("NEW ITEMS: " + str(len(new_items)))

        # Add new items to SAP
        sap_client = SboClient(
            hostname=sap_api.api_server,
            port=sap_api.api_port,
            database=sap_database.db_dbase,
            username=sap_api.api_user,
            password=sap_api.api_passwd,
        )

        for new_item in new_items:
            print("ADDING ITEM: " + new_item.external_id)
            sap_client.item_add(
                new_item.external_id, new_item.name[:200], new_item.id, new_item.price
            )

        # sync items missing in MDAT
        known_items_mdat = list(
            MdatItems.objects.using(mdat.name)
            .filter(created_by_id=settings.MDAT_ROOT_CUSTOMER_ID)
            .values_list("external_id", flat=True)
            .distinct()
        )

        new_items = list(
            Oitm.objects.using(sap_database.name).exclude(itemcode__in=known_items_mdat)
        )

        print("MISSING IN MDAT " + str(len(new_items)))

        for new_item in new_items:
            print("ADDING ITEM: " + new_item.itemcode)
            new_item_mdat = MdatItems(
                created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
                external_id=new_item.itemcode,
                name=new_item.itemname,
                linked_to_id=1,
            )
            new_item_mdat.save(using=mdat.name)
            new_item_mdat.linked_to_id = new_item_mdat.id
            new_item_mdat.save(using=mdat.name)

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
