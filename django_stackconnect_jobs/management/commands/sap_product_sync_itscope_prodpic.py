import json
import urllib.parse

import requests
from django.conf import settings
from django.core.management.base import BaseCommand

from django_sap_business_one_models.django_sap_business_one_models.models import *


def RepresentsInt(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


class Command(BaseCommand):
    help = "Get product pics"

    def handle(self, *args, **options):
        # New Installation Date
        self.stdout.write(self.help)

        prod_wtho_itscopeid = (
            Oitm.objects.using("sapserver")
            .filter(picturname=None)
            .exclude(u_itscopeid=None)
            .order_by("itemcode")
        )

        for item in prod_wtho_itscopeid:
            apiurl = (
                "https://api.itscope.com/2.0/products/id/"
                + str(urllib.parse.quote(item.u_itscopeid, safe=""))
                + "/standard.json?realtime=false"
            )
            r = requests.get(
                apiurl,
                auth=(settings.ITSCOPE_API_AUTH_USER, settings.ITSCOPE_API_AUTH_PASSWD),
            )

            statuscode = r.status_code

            if statuscode == 200:
                json_data = json.loads(r.text)

                if "image1" not in json_data["product"][0]:
                    continue

                item.picturname = (
                    str(urllib.parse.quote(item.itemcode, safe="")) + ".jpg"
                )
                image_url = json_data["product"][0]["image1"]
                fwrite = open("data/prodpics/" + item.picturname, "wb")
                prod_image_r = requests.get(
                    image_url,
                    auth=(
                        settings.ITSCOPE_API_AUTH_USER,
                        settings.ITSCOPE_API_AUTH_PASSWD,
                    ),
                )
                prod_image = prod_image_r.content
                fwrite.write(prod_image)
                fwrite.close()
                item.save()

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
