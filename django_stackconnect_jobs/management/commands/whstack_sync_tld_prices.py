import csv
from pprint import pprint

import pythoncom
import win32com.client
from currency_converter import CurrencyConverter
from django.conf import settings
from django.core.management.base import BaseCommand

from django_stackconnect_jobs.django_stackconnect_jobs.tasks import connect_databases
from django_stackconnect_main.django_stackconnect_main.models import CsExternalDb
from django_stackconnect_sbo.django_stackconnect_sbo.sync_helper import get_item


class Command(BaseCommand):
    help = "Sync TLD items with SAP"

    def handle(self, *args, **options):
        sap_database = CsExternalDb.objects.get(db_type_id=1, id=1)

        connect_databases([sap_database])

        domain_price_list_file = open("data/domain_prices.csv", "rt")

        # skip header
        next(domain_price_list_file)

        domain_price_reader = csv.reader(
            domain_price_list_file, delimiter=",", quotechar='"'
        )

        # SAP Connect
        pythoncom.CoInitialize()

        print("Dispatching COM-Connection")
        vCompany = win32com.client.Dispatch("SAPBobsCOM.Company")
        vCompany.Server = "SWDE0044"
        vCompany.LicenseServer = "SWDE0043:30000"
        vCompany.CompanyDB = sap_database.db_dbase
        vCompany.DbServerType = 10
        vCompany.UserName = settings.SAP_USER
        vCompany.Password = settings.SAP_PASSWORD
        vCompany.UseTrusted = True

        print("Connecting to SAP")
        rtn_cde_connect = vCompany.Connect()

        if rtn_cde_connect:
            print("Unable to connect to SAP: Code " + str(rtn_cde_connect))

            if rtn_cde_connect == 100000048:
                print("No User License")

            if rtn_cde_connect == 2:
                print(
                    "Reboot Computer and delete %temp%\SM_OBS_DLL\ folder and try again"
                )

            if rtn_cde_connect == 14001:
                print("unknown error - delete %temp%\SM_OBS_DLL\ folder and try again")

            if rtn_cde_connect == -10:
                print("NoMsgError - delete %temp%\SM_OBS_DLL\ folder and try again")

            pprint(vCompany.GetLastError())
            pprint(vCompany.GetLastErrorDescription())

            exit(rtn_cde_connect)

        print("Company: " + vCompany.CompanyName)

        for row in domain_price_reader:
            tld = row[0].replace(".", "").upper()
            item_code = "HSDOM" + tld
            item = get_item(sap_database.name, item_code)

            if item is None:
                continue

            # skip rows with monthly costs
            if "monthly" in row[8]:
                continue

            if item is not None:
                print("UPDATE ITEM:" + item_code)

                if row[5] == "EUR":
                    price = float(row[1])
                else:
                    c = CurrencyConverter()
                    price = c.convert(row[1], row[5], "EUR")

                price = price / 12 * 2

                if price < 2:
                    price = 2

                item = vCompany.GetBusinessObject(4)

                item.GetByKey(item_code)

                item.Valid = 1

                item_price = item.PriceList
                item_price.SetCurrentLine(0)
                item_price.Currency = "EUR"
                item_price.Price = price

                item.update()

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
