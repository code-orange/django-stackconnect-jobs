from django.core.mail import send_mail
from django.core.management.base import BaseCommand

from django_sap_business_one_models.django_sap_business_one_models.models import *


class Command(BaseCommand):
    help = "Notify user about products without barcode"

    def handle(self, *args, **options):
        # New Installation Date
        self.stdout.write(self.help)

        all_products_wtho_barcode = (
            Oitm.objects.using("sapserver")
            .exclude(onhand=0)
            .filter(codebars=None)
            .exclude(
                itemcode__in=DolMntItemSkip.objects.values_list("u_itemid", flat=True)
            )
            .order_by("itemcode")
        )

        mail_line = (
            "Automatische Benachrichtigung über "
            + str(len(all_products_wtho_barcode))
            + " Produkte ohne Barcode"
            + "\n\n"
        )

        for p in all_products_wtho_barcode:
            mail_line += "(" + str(p.itemcode) + ") - " + str(p.itemname) + "\n"

        send_mail(
            "Produkte ohne BarCode",
            mail_line,
            "exmaple@example.com",
            ["exmaple1@example.com", "exmaple2@example.com"],
            fail_silently=False,
        )

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
