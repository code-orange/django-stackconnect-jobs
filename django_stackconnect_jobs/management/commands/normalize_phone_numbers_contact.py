import re

from django.conf import settings
from django.core.management.base import BaseCommand
from jira import JIRA

from django_sap_business_one_models.django_sap_business_one_models.models import *


def normalizenr(number):
    number = number.replace("#", "").replace(" ", "").replace(".", "").replace("*", "")
    new_number = number

    if number.startswith("00"):
        new_number = "+" + number[2:]
        # print('             ' + number + " need to be normalized (international) " + new_number)
    elif number.startswith("0137"):
        new_number = "+49" + number[1:]
        # print('             ' + number + " need to be normalized (votecall) " + new_number)
    elif number.startswith("0800"):
        new_number = "+49" + number[1:]
        # print('             ' + number + " need to be normalized (freecall) " + new_number)
    elif number.startswith("0"):
        new_number = "+49" + number[1:]
        # print('             ' + number + " need to be normalized (national) " + new_number)

    return new_number


class Command(BaseCommand):
    help = "Normalize phone numbers in SAP and create a ticket for every change"

    def handle(self, *args, **options):
        # New Installation Date
        self.stdout.write(self.help)

        jira_options = {"server": settings.JIRA_URL, "verify": False}
        jira = JIRA(
            options=jira_options, basic_auth=(settings.JIRA_USER, settings.JIRA_PASSWD)
        )

        all_contacts = Ocpr.objects.using("sapserver").order_by("cntctcode")

        print(all_contacts.query)

        for p in all_contacts:
            contactchange = False

            print()
            print()
            print()
            print()
            print()
            print()

            print("============================")
            print(p.cardcode.cardcode)
            print(p.cardcode.cardname)
            print(p.tel1)
            print(p.tel2)
            print(p.cellolar)
            print(p.fax)
            print("============================")

            ticket_title = (
                "Ansprechpartner aktualisiert: "
                + str(p.cardcode)
                + " - "
                + str(p.cardcode.cardname)
            )
            self.stdout.write(self.style.SUCCESS(ticket_title))

            ticket_description = ""

            if p.tel1 is not None:
                numneu1 = normalizenr(p.tel1)

                if numneu1 != p.tel1:
                    ticket_description += (
                        'Number tel1 changed from "'
                        + str(p.tel1)
                        + '" to "'
                        + str(numneu1)
                        + '"\n'
                    )
                    p.tel1 = numneu1
                    contactchange = True

            if p.tel2 is not None:
                numneu2 = normalizenr(p.tel2)

                if numneu2 != p.tel2:
                    ticket_description += (
                        'Number tel2 changed from "'
                        + str(p.tel2)
                        + '" to "'
                        + str(numneu2)
                        + '"\n'
                    )
                    p.tel2 = numneu2
                    contactchange = True

            if p.fax is not None:
                numneu3 = normalizenr(p.fax)

                if numneu3 != p.fax:
                    ticket_description += (
                        'Number fax changed from "'
                        + str(p.fax)
                        + '" to "'
                        + str(numneu3)
                        + '"\n'
                    )
                    p.fax = numneu3
                    contactchange = True

            if p.cellolar is not None:
                numneu4 = normalizenr(p.cellolar)

                if numneu4 != p.cellolar:
                    ticket_description += (
                        'Number cellolar changed from "'
                        + str(p.cellolar)
                        + '" to "'
                        + str(numneu4)
                        + '"\n'
                    )
                    p.cellolar = numneu4
                    contactchange = True

            if contactchange is True:
                new_issue = jira.create_issue(
                    project="DOL",
                    summary=ticket_title,
                    description=ticket_description,
                    issuetype={"name": "Task"},
                )
                p.save()

            self.stdout.write(self.style.SUCCESS(ticket_description))

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
