import json
import os
import re
import urllib.parse
from datetime import date

import requests
from django.conf import settings
from django.core.management.base import BaseCommand

from django_sap_business_one_models.django_sap_business_one_models.models import *


class Command(BaseCommand):
    help = "Get similar products to match them for their EAN"

    @staticmethod
    def RepresentsInt(s):
        try:
            int(s)
            return True
        except ValueError:
            return False

    def handle(self, *args, **options):
        self.stdout.write(self.help)

        while True:
            skipcount = 0
            prod_wtho_barcode = (
                Oitm.objects.using("sapserver")
                .filter(codebars=None)
                .exclude(
                    itemcode__in=DolMntItemSkip.objects.values_list(
                        "u_itemid", flat=True
                    )
                )
                .order_by("itemcode")
                .first()
            )

            os.system("cls" if os.name == "nt" else "clear")

            self.stdout.write(
                "==================================================================================================================="
            )
            self.stdout.write(
                "  _____        _       _     _         _____               _            _     __  __       _       _               "
            )
            self.stdout.write(
                " |  __ \      | |     | |   (_)       |  __ \             | |          | |   |  \/  |     | |     | |              "
            )
            self.stdout.write(
                " | |  | | ___ | |_ __ | |__  _ _ __   | |__) | __ ___   __| |_   _  ___| |_  | \  / | __ _| |_ ___| |__   ___ _ __ "
            )
            self.stdout.write(
                " | |  | |/ _ \| | '_ \| '_ \| | '_ \  |  ___/ '__/ _ \ / _` | | | |/ __| __| | |\/| |/ _` | __/ __| '_ \ / _ \ '__|"
            )
            self.stdout.write(
                " | |__| | (_) | | |_) | | | | | | | | | |   | | | (_) | (_| | |_| | (__| |_  | |  | | (_| | || (__| | | |  __/ |   "
            )
            self.stdout.write(
                " |_____/ \___/|_| .__/|_| |_|_|_| |_| |_|   |_|  \___/ \__,_|\__,_|\___|\__| |_|  |_|\__,_|\__\___|_| |_|\___|_|   "
            )
            self.stdout.write(
                "                | |                                                                                                "
            )
            self.stdout.write(
                "                |_|                                                                                                "
            )
            self.stdout.write(
                "                                                                                                                   "
            )
            self.stdout.write("SAP Code: " + prod_wtho_barcode.itemcode)
            self.stdout.write("SAP Title: " + prod_wtho_barcode.itemname)

            normalized_name = re.sub(
                "[^a-zA-Z0-9-_*.]", " ", prod_wtho_barcode.itemname
            )
            apiurl = (
                "https://api.itscope.com/2.0/products/search/keywords%3D"
                + str(urllib.parse.quote(normalized_name, safe=""))
                + "/standard.json?realtime=false&plzproducts=false&page=1&item=0&sort=DEFAULT"
            )
            r = requests.get(
                apiurl,
                auth=(settings.ITSCOPE_API_AUTH_USER, settings.ITSCOPE_API_AUTH_PASSWD),
            )

            self.stdout.write("SAP Title normalized: " + normalized_name)
            self.stdout.write("API-URL: " + apiurl)
            self.stdout.write(
                "==================================================================================================================="
            )

            statuscode = r.status_code

            if statuscode == 200:
                json_data = json.loads(r.text)
                result_count = 10

                if len(json_data["product"]) < result_count:
                    result_count = len(json_data["product"])

                for i in range(result_count):
                    self.stdout.write("---")

                    if "price" not in json_data["product"][i]:
                        json_data["product"][i]["price"] = 0

                    if "stock" not in json_data["product"][i]:
                        json_data["product"][i]["stock"] = 0

                    if "manufacturerSKU" not in json_data["product"][i]:
                        json_data["product"][i]["manufacturerSKU"] = 0

                    if "ean" not in json_data["product"][i]:
                        skipcount += 1
                        continue

                    self.stdout.write(
                        str(i)
                        + " - ITscope EAN: "
                        + str(json_data["product"][i]["ean"])
                    )
                    self.stdout.write(
                        str(i)
                        + " - ITscope Title: "
                        + str(json_data["product"][i]["manufacturerSKU"])
                        + " - "
                        + str(json_data["product"][i]["productNameWithManufacturer"])
                    )
                    self.stdout.write(
                        str(i)
                        + " - ITscope Price: "
                        + str(json_data["product"][i]["price"])
                    )
                    self.stdout.write(
                        str(i)
                        + " - ITscope Stock: "
                        + str(json_data["product"][i]["stock"])
                    )

                if skipcount == result_count:
                    self.stdout.write(self.style.SUCCESS("No Product found."))
                    prod_skip = DolMntItemSkip(
                        u_itemid=prod_wtho_barcode,
                        u_reason="itscope products found but no ean",
                    )
                    prod_skip.save()
                    continue

                userchoice = input("Is this a match? (s)kip, [0-9]: ")

                if self.RepresentsInt(userchoice):
                    choiceint = int(userchoice)
                    prod_wtho_barcode.codebars = json_data["product"][choiceint]["ean"]
                    onnm_entity = Onnm.objects.using("sapserver").get(
                        objectcode="1470000062"
                    )  # Obj 1470000062 = Codebar IDs
                    onnm_entity.refresh_from_db()  # make sure, AutoKey is Up2Date
                    obcd_entity = Obcd(
                        bcdentry=onnm_entity.autokey,
                        bcdcode=prod_wtho_barcode.codebars,
                        itemcode=prod_wtho_barcode,
                        uomentry=-1,
                        datasource="N",
                        loginstanc=0,
                        usersign2=4,
                        updatedate=date.today(),
                    )
                    onnm_entity.autokey += 1
                    onnm_entity.save()
                    obcd_entity.save(force_insert=True, using="sapserver")
                    prod_wtho_barcode.save()
                else:
                    prod_skip = DolMntItemSkip(
                        u_itemid=prod_wtho_barcode, u_reason="user skipped match"
                    )
                    prod_skip.save()

            if statuscode == 404:
                self.stdout.write(self.style.SUCCESS("No Product found."))
                prod_skip = DolMntItemSkip(
                    u_itemid=prod_wtho_barcode, u_reason="itscope http 404"
                )
                prod_skip.save()

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
