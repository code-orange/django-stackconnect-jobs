from pprint import pprint
from urllib.parse import urljoin

import pythoncom
import requests
import win32com.client
from django.conf import settings
from django.core.management.base import BaseCommand

from django_stackconnect_jobs.django_stackconnect_jobs.tasks import connect_databases
from django_stackconnect_main.django_stackconnect_main.models import CsExternalDb
from django_stackconnect_sbo.django_stackconnect_sbo.sync_helper import get_item


class Command(BaseCommand):
    help = "Sync TLD items with SAP"

    def handle(self, *args, **options):
        sap_database = CsExternalDb.objects.get(db_type_id=1, id=1)

        connect_databases([sap_database])

        request_url = urljoin(settings.WHSTACK_API_URL, "api/v1/whstacktld")

        session = requests.Session()
        session.headers = {
            "Authorization": "ApiKey "
            + settings.WHSTACK_API_USER
            + ":"
            + settings.WHSTACK_API_PASSWD,
            "Content-Type": "application/json",
            "Accept": "application/json",
        }

        # create arguments for the request
        request_args = {
            "url": request_url,
            "verify": False,
        }

        # do the request
        response = session.get(**request_args)
        response_data = response.json()
        tlds = response_data["objects"]

        # SAP Connect
        pythoncom.CoInitialize()

        print("Dispatching COM-Connection")
        vCompany = win32com.client.Dispatch("SAPBobsCOM.Company")
        vCompany.Server = "SWDE0044"
        vCompany.LicenseServer = "SWDE0043:30000"
        vCompany.CompanyDB = sap_database.db_dbase
        vCompany.DbServerType = 10
        vCompany.UserName = settings.SAP_USER
        vCompany.Password = settings.SAP_PASSWORD
        vCompany.UseTrusted = True

        print("Connecting to SAP")
        rtn_cde_connect = vCompany.Connect()

        if rtn_cde_connect:
            print("Unable to connect to SAP: Code " + str(rtn_cde_connect))

            if rtn_cde_connect == 100000048:
                print("No User License")

            if rtn_cde_connect == 2:
                print(
                    "Reboot Computer and delete %temp%\SM_OBS_DLL\ folder and try again"
                )

            if rtn_cde_connect == 14001:
                print("unknown error - delete %temp%\SM_OBS_DLL\ folder and try again")

            if rtn_cde_connect == -10:
                print("NoMsgError - delete %temp%\SM_OBS_DLL\ folder and try again")

            pprint(vCompany.GetLastError())
            pprint(vCompany.GetLastErrorDescription())

            exit(rtn_cde_connect)

        print("Company: " + vCompany.CompanyName)

        for tld_object in tlds:
            tld = tld_object["tld"].lower()
            item_code = "HSDOM" + tld.upper()

            pprint(get_item(sap_database.name, item_code))

            # TODO: implement XN domains
            if tld.startswith("xn--"):
                continue

            # continue if item already existent
            if get_item(sap_database.name, item_code) is not None:
                continue

            print("Creating: " + item_code)

            item = vCompany.GetBusinessObject(4)

            item.ItemCode = item_code
            item.Valid = 1
            item.WhsInfo.WarehouseCode = "01"
            item.ItemName = "Hosting Domain ." + tld + " (mtl.)"
            item.InventoryItem = False
            item.PurchaseItem = False

            item_price = item.PriceList
            item_price.SetCurrentLine(0)
            item_price.Currency = "EUR"
            item_price.Price = 999999

            item.add()

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
