from django.core.management.base import BaseCommand

from django_sap_business_one_models.django_sap_business_one_models.models import *


class Command(BaseCommand):
    help = "Set E-Invoice E-Mail to main E-Mail if empty"

    def handle(self, *args, **options):
        # New Installation Date
        self.stdout.write(self.help)

        all_customers_wtho_einv_email = Ocrd.objects.using("sapserver").filter(
            u_dol_einv_email__isnull=True
        )

        for customer in all_customers_wtho_einv_email:
            print("Setting E-Inv E-Mail for " + str(customer.cardname))
            customer.u_dol_einv_email = customer.e_mail
            customer.save()

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
