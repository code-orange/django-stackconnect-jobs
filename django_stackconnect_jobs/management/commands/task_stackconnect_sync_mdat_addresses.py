from django.core.management.base import BaseCommand

from django_stackconnect_jobs.django_stackconnect_jobs.tasks import (
    stackconnect_sync_mdat_addresses,
)


class Command(BaseCommand):
    def handle(self, *args, **options):
        stackconnect_sync_mdat_addresses()
