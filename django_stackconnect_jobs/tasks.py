from __future__ import absolute_import, unicode_literals

from time import mktime
from itertools import chain

import dateutil.parser
import requests
from celery import shared_task
from django.conf import settings
from django.db import connections, transaction

from django_master_data_backend_client.django_master_data_backend_client.func import (
    master_data_backend_create_customer,
    master_data_backend_get_customer,
)
from django_sap_business_one_models.django_sap_business_one_models.models import *
from django_stackconnect_main.django_stackconnect_main.models import *
from django_stackconnect_sbo.django_stackconnect_sbo.sync_helper import (
    get_appointments as sbo_get_appointments,
)
from django_stackconnect_sbo.django_stackconnect_sbo.sync_helper import (
    get_customers as sbo_get_customers,
)
from django_stackconnect_sbo.django_stackconnect_sbo.sync_helper import (
    get_items as sbo_get_items,
)
from django_svcstack_models.django_svcstack_models.models import *


def connect_databases(databases: list):
    for external_db in databases:
        database = external_db.name

        new_database = dict()
        new_database["id"] = database
        new_database["NAME"] = external_db.db_dbase
        new_database["ENGINE"] = external_db.db_engine
        new_database["USER"] = external_db.db_user
        new_database["PASSWORD"] = external_db.db_passwd
        new_database["HOST"] = external_db.db_ip
        new_database["TIME_ZONE"] = "Europe/Berlin"
        new_database["CONN_HEALTH_CHECKS"] = False
        new_database["CONN_MAX_AGE"] = 0
        new_database["AUTOCOMMIT"] = True
        new_database["OPTIONS"] = dict()

        if external_db.db_engine == "django.db.backends.mysql":
            new_database["PORT"] = 3306
            new_database["OPTIONS"][
                "init_command"
            ] = "SET sql_mode='STRICT_TRANS_TABLES'"
        elif external_db.db_engine == "mssql":
            new_database["PORT"] = 1433
            new_database["OPTIONS"]["driver"] = external_db.db_driver
            new_database["OPTIONS"]["host_is_server"] = True
        else:
            pass

        connections.databases[database] = new_database


@shared_task(name="stackconnect_sync_customers")
def stackconnect_sync_customers():
    # Connect to DBs
    # TODO: remove hardcoded IDs
    all_customer_dbs = CsExternalDb.objects.filter(db_type_id=1, id=1).order_by("id")

    connect_databases(all_customer_dbs)

    for customer_db in all_customer_dbs:
        source_db_name = customer_db.name

        for new_customer in sbo_get_customers(database=source_db_name):
            customer_data = dict()
            customer_data["external_id"] = new_customer.external_id
            customer_data["name"] = new_customer.name
            customer_data["enabled"] = new_customer.enabled
            customer_data["primary_email"] = new_customer.primary_email
            customer_data["billing_email"] = new_customer.billing_email
            customer_data["tech_email"] = new_customer.tech_email

            master_data_backend_create_customer(
                settings.MDAT_ROOT_CUSTOMER_ID, customer_data
            )

    return


@shared_task(name="stackconnect_sync_items")
def stackconnect_sync_items():
    # Connect to DBs
    # TODO: remove hardcoded IDs
    all_item_dbs = CsExternalDb.objects.filter(db_type_id=1, id=1).order_by("id")
    all_md_dbs = CsExternalDb.objects.filter(id=15).order_by("id")

    connect_databases(list(chain(all_item_dbs, all_md_dbs)))

    for item_db in all_item_dbs:
        source_db_name = item_db.name

        for new_item in sbo_get_items(database=source_db_name):
            print("SYNC: " + new_item.itemcode)

            try:
                remote_item = MdatItems.objects.using(all_md_dbs[0].name).get(
                    external_id=new_item.itemcode,
                    created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
                )
            except MdatItems.DoesNotExist:
                remote_item = MdatItems(
                    external_id=new_item.itemcode,
                    created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
                )

            if remote_item.linked_to_id is None:
                remote_item.linked_to_id = 100000

            if remote_item.discount_percent is None:
                remote_item.discount_percent = 0

            remote_item.name = new_item.itemname

            # price
            # TODO: write helper
            item_price = Itm1.objects.using(source_db_name).get(
                itemcode=new_item.itemcode,
                pricelist=1,
            )

            remote_item.price = item_price.price

            if remote_item.price is None:
                remote_item.price = 0

            remote_item.save(using=all_md_dbs[0].name)

            remote_item.linked_to = remote_item

            remote_item.save(using=all_md_dbs[0].name)

    return


@shared_task(name="stackconnect_sync_appointments")
def stackconnect_sync_appointments():
    # Connect to DBs
    # TODO: remove hardcoded IDs
    all_appointment_dbs = CsExternalDb.objects.filter(db_type_id=1, id=1).order_by("id")
    all_svcstack_dbs = CsExternalDb.objects.filter(db_type_id=2, id=3).order_by("id")

    connect_databases(list(chain(all_appointment_dbs, all_svcstack_dbs)))

    for customer_db in all_appointment_dbs:
        for destination_db in all_svcstack_dbs:
            source_db_name = customer_db.name
            destination_db_name = destination_db.name

            for new_appointment in sbo_get_appointments(database=source_db_name):
                new_appointment.customer = MdatCustomers.objects.using(
                    destination_db_name
                ).get(external_id=str(new_appointment.customer_temp_id))

                try:
                    old_appointment = SvcstackActivities.objects.using(
                        destination_db_name
                    ).get(
                        source_id=new_appointment.source_id,
                        source_extid=new_appointment.source_extid,
                    )
                except SvcstackActivities.DoesNotExist:
                    pass
                else:
                    new_appointment.id = old_appointment.id

                new_appointment.save(using=destination_db_name)

    return


@shared_task(name="stackconnect_sync_cdr_to_crm")
def stackconnect_sync_cdr_to_crm():
    # TODO: Move code to abstract class

    # Connect to DBs
    # TODO: remove hardcoded IDs
    all_appointment_dbs = CsExternalDb.objects.filter(db_type_id=1, id=1).order_by("id")
    connect_databases(all_appointment_dbs)

    blacklist_nums = []

    api_url = settings.DOLPHIN_CONNECT_HOTEL_API_URL + "/v1/crm"

    crm_request = requests.get(
        api_url + "/sip999999",
        headers={
            "Authorization": (
                "ApiKey "
                + settings.DOLPHIN_CONNECT_HOTEL_API_USERNAME
                + ":"
                + settings.DOLPHIN_CONNECT_HOTEL_API_KEY
            )
        },
    )

    for call_id, call_details in crm_request.json().items():
        print("Processing CallID: " + str(call_id))

        dbase = all_appointment_dbs.first().name
        blacklist_hit = False

        for bl_num in blacklist_nums:
            if call_details["num_src"] == bl_num or call_details["num_dst"] == bl_num:
                blacklist_hit = True

        if blacklist_hit is True:
            continue

        call_details["call_start"] = dateutil.parser.parse(call_details["call_start"])
        call_details["call_end"] = dateutil.parser.parse(call_details["call_end"])

        with transaction.atomic(using=dbase):
            if call_details["direction"] == "out":
                num_leg_dolphin = call_details["num_src"]
                num_leg_customer = call_details["num_dst"]
            else:
                num_leg_dolphin = call_details["num_dst"]
                num_leg_customer = call_details["num_src"]

            data_source = "contact"

            contact = (
                Ocpr.objects.using(dbase)
                .filter(
                    Q(tel1=num_leg_customer)
                    | Q(tel2=num_leg_customer)
                    | Q(fax=num_leg_customer)
                    | Q(cellolar=num_leg_customer)
                )
                .order_by("cardcode")
            )

            if len(contact) <= 0:
                data_source = "bpartner"

                bpartner = (
                    Ocrd.objects.using(dbase)
                    .filter(
                        Q(phone1=num_leg_customer)
                        | Q(phone2=num_leg_customer)
                        | Q(fax=num_leg_customer)
                        | Q(cellular=num_leg_customer)
                    )
                    .order_by("cardcode")
                )

                if len(bpartner) <= 0:
                    print("Number not found: " + num_leg_customer)
                    continue

            # set internal representative
            rep_list = (
                Ohem.objects.using(dbase)
                .filter(
                    Q(officetel=num_leg_dolphin)
                    | Q(officeext=num_leg_dolphin)
                    | Q(mobile=num_leg_dolphin)
                    | Q(pager=num_leg_dolphin)
                    | Q(hometel=num_leg_dolphin)
                )
                .order_by("userid")
            )

            if rep_list.count() <= 0:
                print("REP not found: " + num_leg_dolphin)
                rep = Ohem.objects.using(dbase).get(empid=2)
            else:
                rep = rep_list.first()

            onnm_entity = Onnm.objects.using(dbase).get(
                objectcode="33"
            )  # Obj 33 = Activity ID
            onnm_entity.refresh_from_db()  # make sure, AutoKey is Up2Date

            # Duration
            # Convert to Unix timestamp
            d1_ts = mktime(call_details["call_start"].timetuple())
            d2_ts = mktime(call_details["call_end"].timetuple())

            new_activity = Oclg(
                clgcode=onnm_entity.autokey,
                attenduser=rep.userid,
                usersign=rep.userid.userid,
                assignedby=rep.userid.userid,
                cntctdate=call_details["call_start"],
                cntcttime=call_details["call_start"].strftime("%H%M"),
                recontact=call_details["call_start"],
                tel=num_leg_customer,
                closed="N",
                cntctsbjct=-1,
                transfered="N",
                doctype=-1,
                datasource="I",
                slpcode=-1,
                action="C",
                cntcttype=Oclt.objects.using(dbase).get(code=-1),
                location=-1,
                begintime=call_details["call_start"].strftime("%H%M"),
                duration=int(d2_ts - d1_ts) / 60,
                durtype="M",
                endtime=call_details["call_end"].strftime("%H%M"),
                priority=1,
                reminder="N",
                remqty=15.000000,
                remtype="M",
                remsented="N",
                instance=0,
                enddate=call_details["call_end"],
                personal="N",
                inactive="N",
                tentative="N",
                recurpat="N",
                endtype="N",
                sestartdat=call_details["call_start"],
                interval=1,
                sunday="N",
                monday="N",
                tuesday="N",
                wednesday="N",
                thursday="N",
                friday="N",
                saturday="N",
                suboption=1,
                isremoved="N",
                addrtype="S",
                u_startchanged=0,
                u_endchanged=0,
                u_ntfrallusers=0,
            )

            if data_source == "contact":
                contact_first = contact.first()
                print("Call: " + num_leg_customer + " Name: " + contact_first.name)

                # Activity summary
                new_activity.details = str(
                    "Telefonat / Fernwartung mit " + contact_first.name
                )[:49]

                new_activity.cardcode = contact_first.cardcode
                new_activity.cntctcode = contact_first
            elif data_source == "bpartner":
                bpartner_first = bpartner.first()
                print("Call: " + num_leg_customer + " Name " + bpartner_first.cardname)

                # Activity summary
                new_activity.details = str(
                    "Telefonat / Fernwartung mit " + bpartner_first.cardname
                )[:49]

                new_activity.cardcode = bpartner_first
            else:
                print("call direction unknown")

            onnm_entity.autokey += 1
            onnm_entity.save()

            new_activity.save(force_insert=True, using=dbase)

            crm_flag = requests.post(
                api_url,
                headers={
                    "Authorization": (
                        "ApiKey "
                        + settings.DOLPHIN_CONNECT_HOTEL_API_USERNAME
                        + ":"
                        + settings.DOLPHIN_CONNECT_HOTEL_API_KEY
                    )
                },
                json=dict(
                    call_id=call_id,
                ),
            )

    return


@shared_task(name="stackconnect_sync_reseller_price_list")
def stackconnect_sync_reseller_price_list():
    # TODO: Move code to abstract class

    # Connect to DBs
    # TODO: remove hardcoded IDs
    all_sap_dbs = CsExternalDb.objects.filter(db_type_id=1, id=1).order_by("id")
    connect_databases(all_sap_dbs)
    dbase = all_sap_dbs.first().name

    all_domains = Oitm.objects.using(dbase).filter(itemcode__istartswith="HSDOM")

    for dom in all_domains:
        item = Itm1.objects.using(dbase).get(itemcode=dom.itemcode, pricelist=1)

        try:
            special_price = Ospp.objects.using(dbase).get(
                cardcode=103031, itemcode=dom.itemcode
            )
        except Ospp.DoesNotExist:
            special_price = Ospp(
                itemcode=dom.itemcode,
                cardcode=103031,
                price=item.price,
                currency=item.currency,
                discount=50,
                listnum=1,
                autoupdt="N",
                expand="N",
                usersign=5,
                srcprice=0,
                loginstanc=0,
                usersign2=4,
                createdate=datetime.now(),
                updatedate=datetime.now(),
                valid="Y",
            )
            special_price.save(force_insert=True, using=dbase)

    return


@shared_task(name="stackconnect_sync_mdat_customers")
def stackconnect_sync_mdat_customers():
    # Connect to DBs
    # TODO: remove hardcoded IDs
    all_md_dbs = CsExternalDb.objects.filter(db_type_id=20, id=15).order_by("id")
    all_svcstack_dbs = CsExternalDb.objects.filter(
        db_type_id__in=(2, 3, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 18, 19)
    ).order_by("id")

    connect_databases(list(chain(all_md_dbs, all_svcstack_dbs)))

    for customer_db in all_md_dbs:
        for destination_db in all_svcstack_dbs:
            source_db_name = customer_db.name
            destination_db_name = destination_db.name

            print("===== DB: " + destination_db_name)

            for customer_source in MdatCustomers.objects.using(source_db_name).all():
                try:
                    remote_customer = MdatCustomers.objects.using(
                        destination_db_name
                    ).get(id=customer_source.id)
                except MdatCustomers.DoesNotExist:
                    remote_customer = customer_source
                    remote_customer.id = customer_source.id
                    remote_customer.save(using=destination_db_name, force_insert=True)

                remote_customer = customer_source
                remote_customer.id = customer_source.id
                remote_customer.save(using=destination_db_name, force_update=True)

    return


@shared_task(name="stackconnect_sync_mdat_items")
def stackconnect_sync_mdat_items():
    # Connect to DBs
    # TODO: remove hardcoded IDs
    all_md_dbs = CsExternalDb.objects.filter(db_type_id=20, id=15).order_by("id")
    all_svcstack_dbs = CsExternalDb.objects.filter(
        db_type_id__in=(2, 3, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 18, 19)
    ).order_by("id")

    connect_databases(list(chain(all_md_dbs, all_svcstack_dbs)))

    for item_db in all_md_dbs:
        for destination_db in all_svcstack_dbs:
            source_db_name = item_db.name
            destination_db_name = destination_db.name

            print("===== DB: " + destination_db_name)

            for item_source in MdatItems.objects.using(source_db_name).all():
                try:
                    remote_item = MdatItems.objects.using(destination_db_name).get(
                        id=item_source.id
                    )
                except MdatItems.DoesNotExist:
                    remote_item = item_source
                    remote_item.id = item_source.id
                    remote_item.save(using=destination_db_name, force_insert=True)

                remote_item = item_source
                remote_item.id = item_source.id
                remote_item.save(using=destination_db_name, force_update=True)

    return


@shared_task(name="stackconnect_sync_mdat_addresses")
def stackconnect_sync_mdat_addresses():
    # Connect to DBs
    # TODO: remove hardcoded IDs
    all_sap_dbs = CsExternalDb.objects.filter(db_type_id=1, id=1).order_by("id")
    all_md_dbs = CsExternalDb.objects.filter(db_type_id=20, id=15).order_by("id")
    connect_databases(list(chain(all_sap_dbs, all_md_dbs)))

    dbase = all_sap_dbs.first().name
    mdat_dbase = all_md_dbs.first().name

    for customer in Ocrd.objects.using(dbase).all():
        print(f"PROCESSING {customer.cardcode}")

        mdat_customer = MdatCustomers.objects.using(mdat_dbase).get(
            created_by_id=settings.MDAT_ROOT_CUSTOMER_ID,
            external_id=customer.cardcode,
        )

        mdat = master_data_backend_get_customer(
            settings.MDAT_ROOT_CUSTOMER_ID, mdat_customer.id
        )

        for address_type in ("S", "B"):
            mdat_attribute = "delivery_address"

            if address_type == "B":
                mdat_attribute = "billing_address"

            mdat_adr = mdat[mdat_attribute]

            Crd1.objects.using(dbase).filter(cardcode_id=customer.cardcode).filter(
                adrestype=address_type
            ).filter(address=customer.cardcode).update(
                street=mdat_adr["street"]["title"] + " " + mdat_adr["house_nr"],
                zipcode=mdat_adr["street"]["city"]["zip_code"],
                city=mdat_adr["street"]["city"]["title"],
                country=mdat_adr["street"]["city"]["country"]["iso"],
                loginstanc=0,
                objtype=2,
                linenum=0,
                taasenbl="Y",
                createdate=datetime.now(),
                createts=0,
            )

    return
